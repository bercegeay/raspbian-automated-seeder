#!/usr/bin/env python
import RPi.GPIO as GPIO
import time 


ObstaclePin = 11

def setup():
	GPIO.setmode(GPIO.BOARD)       # Numbers GPIOs by physical location
	GPIO.setup(ObstaclePin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
	GPIO.setwarnings(False)

def turn():

	GPIO.setmode(GPIO.BOARD)
	GPIO.setup(12, GPIO.OUT)
	p = GPIO.PWM(12, 100)
	p.start(7.5)

	try:

        	p.ChangeDutyCycle(70)
        	time.sleep(2)
        	p.ChangeDutyCycle(10)
		time.sleep(2)

	except KeyboardInterrupt:
        	p.stop()
        	GPIO.cleanup()


def loop():
	while True:
		if (0 == GPIO.input(ObstaclePin)):
			print 'Detected Barrier!'
			turn()
			

def destroy():
	GPIO.cleanup()                     # Release resource

if __name__ == '__main__':     # Program start from here
	setup()
	try:
		loop()
	except KeyboardInterrupt:  # When 'Ctrl+C' is pressed, the child program destroy() will be  executed.
		destroy()

